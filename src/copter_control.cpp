/*
*   Nodo que recibirá velocidades lineales y posicion en Z y realizara un 
*   control desacoplado por simpleza a través de un P.
*
*/

#include "ros/ros.h"


#include "geometry_msgs/Twist.h"
#include "geometry_msgs/Pose.h"
#include "geometry_msgs/Point.h"
#include "geometry_msgs/Vector3.h"
#include "std_msgs/Float64.h"

#include <iostream>
#include <fstream>
#include <math.h>
#include <stdlib.h>
#include <sstream>

//global variables
geometry_msgs::Twist latestVel;
geometry_msgs::Point latestPos;
geometry_msgs::Vector3 control;//Angle Control Actions
std_msgs::Float64 W;

void velCallback(const geometry_msgs::Twist::ConstPtr & message)
{
  latestVel=*message;
}
void poseCallback(const geometry_msgs::Point::ConstPtr & message)
{
  latestPos =*message;
}
int main(int argc, char **argv)
{
  
  ros::init(argc, argv, "Control");

  
  ros::NodeHandle n;
  
  ros::Publisher ctrl_pub = n.advertise<geometry_msgs::Vector3>("copter_model/AngleControl", 100);
  ros::Publisher Alt_ctrl_pub = n.advertise<std_msgs::Float64>("copter_model/ZControl", 100);
  
  ros::Subscriber Velsub = n.subscribe("copter_model/Vel", 1000, velCallback);
  ros::Subscriber Posesub = n.subscribe("copter_model/Pose", 1000, poseCallback);
 
  ROS_INFO("Node: 'Control' ready");
  ros::Rate loop_rate(1000);//1000 veces por segundo
  float K=10;
  float Ka=1;
  //References:
  float RefVelx=1;
  float RefVely=1;
  float RefAltitude=-2;//para subir un metro
   while (ros::ok())
	{
  //calculamos los parametros del control:
  /* Desacoplamos el control y hacemos un control P para cada accion de control
  *  Control en velocidad: cada angulo del Swash-plate sera proporcional al error de la velocidad en x o y
  *                        La velocidad angular de los rotores sera igual para ambos pero en funcion del error con
  *                        la altura.
  */
  control.x=K*(RefVelx-latestVel.linear.x);
  control.y=K*(RefVely-latestVel.linear.y);
  control.z=0;

  //Altitud
  W.data=Ka*(-RefAltitude+latestPos.z) +1469.5;//lo que se suma es la velocidad que vence la gravedad
  //if(W.data<=1469.5)W.data=1469.5;
  //Publicamos las acciones de control:
  ctrl_pub.publish(control);
  Alt_ctrl_pub.publish(W);

  ros::spinOnce();

	loop_rate.sleep();
}

  return 0;
}